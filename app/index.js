/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
{/**
import React, {Component} from 'react';
import Router from './routes';
import {AppRegistry} from 'react-native';

export default class CustomDrawer extends Component {
  render () {
    return (
      <Router/>
    );
  }
}

AppRegistry.registerComponent('CustomDrawer', () => CustomDrawer);**/}

import React, {Component} from 'react';
import {Text, Image} from 'react-native'
import {Container, Content, Header, Left, Body, Icon} from 'native-base'
import {createDrawerNavigator, DrawerItems} from 'react-navigation'
import Ionicons from 'react-native-vector-icons/Ionicons'

import HomeComp from './app/HomeComp'
import ProfileCom from './app/ProfileComp'

const myNav = createDrawerNavigator({
  Home: HomeComp,
  Profile: ProfileComp

  })
