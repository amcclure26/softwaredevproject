{/*
  Software Engineering Proect
  -Adrian Edits
  */}

import React from 'react';
import {StyleSheet, Button, View, SafeAreaView, Text, Alert, Image, AppRegistry} from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { createSwitchNavigator } from 'react-navigation';

{/* Home Screen - (Login Page) */}
export default class ProfileComp extends Component{
  render(){
    return(
      <Container>
      <Content contentContainerStyle={{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
      }}>
      <Text>'This is text profile Component'</Text>
      </Content>
      </Container>
    )
  }

}
