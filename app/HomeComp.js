{/*
  Software Engineering Proect
  -Adrian Edits
  */}

import {StyleSheet, Button, View, SafeAreaView, Text, Alert, Image, AppRegistry} from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { createSwitchNavigator } from 'react-navigation';
import React, {Component} from 'react';
import {Container, Content, Header, Left, Body, Icon} from 'native-base'
import {createDrawerNavigator, DrawerItems} from 'react-navigation'
import Ionicons from 'react-native-vector-icons/Ionicons'

{/* Home Screen - (Login Page) */}
export default class HomeComp extends Component{
  render(){
    return(
      <Container>
      <Content contentContainerStyle={{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
      }}>
      <Text>'This is text Component'</Text>
      </Content>
      </Container>
    )
  }

}
