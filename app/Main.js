{/*
  Software Engineering Proect
  -Adrian Edits
  */}

import React from 'react';
import {StyleSheet, Button, View, SafeAreaView, Text, Alert, Image, AppRegistry} from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { createSwitchNavigator } from 'react-navigation';

{/* Home Screen - (Login Page) */}

class HomeScreen extends React.Component {
  render() {
    let pic = {
      uri: 'http://freevector.co/wp-content/uploads/2013/12/33838-basketball-ball-variant.png'
    };
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text style={styles.titleText} >Court Keeper</Text>
        <Image source={pic} style={{width: 200, height: 200}}/>

        <Button
          title="Login"
          onPress={() => this.props.navigation.navigate('Details')}
        />
      </View>
    );
  }
}

{/* Details Screen - (After Login) */}
class DetailsScreen extends React.Component {
  render() {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>Details Screen</Text>
      </View>
    );
  }
}

class SettingsScreen extends React.Component {
  render() {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Text>Settings!</Text>
      </View>
    );
  }
}

const TabNavigator = createBottomTabNavigator(
  {
  Details: {screen: DetailsScreen},
  Settings: {screen: SettingsScreen},
  },
);

const RootStack = createStackNavigator(
  {
    Home: {screen: HomeScreen},
    Details: {screen: DetailsScreen},
  },
  {
    initialRouteName: 'Home',
  });


const AppContainer = createAppContainer(RootStack, TabNavigator);

export default class App extends React.Component {
  render() {
    return <AppContainer />;
  }
}

//export default createAppContainer(TabNavigator);

{/* Style SHeets */}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  titleText: {
    fontSize: 30,
    fontWeight: 'bold',
  }
});
