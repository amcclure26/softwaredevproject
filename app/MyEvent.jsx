import React, {Component} from 'react';
import { Text, View, StyleSheet, Button, Alert, Image} from 'react-native';

export class MyEvent extends Component{
  render(){
    return(
      <View style={styles.container}>
        <View style={styles.header}/>

        <View style={styles.buttonwrapper}>
          <Button title='Home'
                  onPress={() => Alert.alert('Home Button Pressed')}/>
          <Button title='Edit'
                  onPress={() => Alert.alert('Edit Button Pressed')}/>
        </View>

        <Text>{'\n'}</Text>

        <View style={styles.card}>
          <Image source={{uri: 'http://placehold.it/100x100'}}
                style={{height: 89, width: 90}}/>
          <View style={styles.textView}>
            <Text> Event Name</Text>
            <Text> Creator</Text>
            <Text> Location</Text>
            <Text> Time</Text>
          </View>
        </View>

        <Text>{'\n'}</Text>

        <View style={styles.card}>
          <Image source={{uri: 'http://placehold.it/100x100'}}
                style={{height: 89, width: 90}}/>
          <View style={styles.textView}>
            <Text> Event Name</Text>
            <Text> Creator</Text>
            <Text> Location</Text>
            <Text> Time</Text>
          </View>
        </View>

        <Text>{'\n'}</Text>

        <View style={styles.card}>
          <Image source={{uri: 'http://placehold.it/100x100'}}
                style={{height: 89, width: 90}}/>
          <View style={styles.textView}>
            <Text> Event Name</Text>
            <Text> Creator</Text>
            <Text> Location</Text>
            <Text> Time</Text>
          </View>
        </View>
        
        <Text>{'\n'}</Text>

        <View style={styles.card}>
          <Image source={{uri: 'http://placehold.it/100x100'}}
                style={{height: 89, width: 90}}/>
          <View style={styles.textView}>
            <Text> Event Name</Text>
            <Text> Creator</Text>
            <Text> Location</Text>
            <Text> Time</Text>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {

  },
  header: {
    height: 50,
    backgroundColor: 'steelblue'
  },
  buttonwrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  card: {
    height: 90,
    width: 250,
    backgroundColor: 'lightgray',
    alignSelf: 'center',
    flexDirection: 'row',
    borderWidth: 0.5,
    borderColor: 'black'
  },
  textView: {
    justifyContent: 'center',
    alignItems: 'flex-start'
  }
});