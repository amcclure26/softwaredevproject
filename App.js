{/*
  Software Engineering Proect
  -Adrian Edits
  */}

import React from 'react';
import { StyleSheet, Modal, Button, AsyncStorage, View, SafeAreaView, TouchableOpacity, Text, Alert, Image, AppRegistry, TextInput, ScrollView, Picker, KeyboardAvoidingView, TouchableHighlight } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { createSwitchNavigator } from 'react-navigation';
import { createDrawerNavigator, DrawerItems } from 'react-navigation-drawer';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Constants from 'expo-constants';
import { List, Overlay, SearchBar, ListItem, Badge, Card, Icon, Divider, CheckBox, Input } from 'react-native-elements';
import QRCode from 'react-native-qrcode-svg';
import ModalDropdown from 'react-native-modal-dropdown'
import PasswordInputText from 'react-native-hide-show-password-input';
import axios from 'axios'

import Routes from './routes'
//import {MyEvent} from './app/MyEvent';

{/* Home Screen - (Login Page) */ }
//const routes = new Routes();
//var currUser = {};
const url = "http://nameless-everglades-94891.herokuapp.com";
console.disableYellowBox = true;
const config = {}
var token = '';
var user = '';
var qr = '';
var currEvent = {}
var myList = []
eventList = [
  {
    avatar: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRjIjvdD3nRJ6Gq6qf-5z84B-sNTWnhHas1S7cbZjeh8G4mSsmk&sg',
    type: 'Tennis',
    eventname: 'Tennis 2019',
    creator: 'Callie',
    location: 'Tennis Field',
    time: '7:00pm - 8:30pm'
  },
  {
    avatar: 'https://sunbeltpools.com/wp-content/uploads/2016/03/3-1.jpg',
    type: 'Pool',
    eventname: 'Water Fight',
    creator: 'Rian',
    location: 'Dedman Center',
    time: '9:30am - 10:30am'
  },
  {
    avatar:'https://pbs.twimg.com/profile_images/938900189972013057/UUD2RQPq_400x400.jpg',
    type: 'Pool',
    eventname: 'Pool Con',
    creator: 'Arandeep',
    location: 'Dedman Center',
    time: '3:00pm - 4:00pm'
  },
  {
    avatar:'https://sunbeltpools.com/wp-content/uploads/2016/03/3-1.jpg',
    type: 'Others',
    eventname: 'The Running Honors',
    creator: 'Benas',
    location: 'Stadium',
    time: '6:30am - 7:30am'
  },
  {
    avatar:'https://www.smudailycampus.com/wp-content/uploads/2019/02/DedmanCenter-360x258.jpg',
    type: 'Others',
    eventname: 'Hockey Charity',
    creator: 'Jiya',
    location: 'Dedman Center',
    time: '8:30pm - 10:30pm'
  },
  {
    avatar:'https://www.smu.edu/-/media/Site/News/NewsSources/SMUat100/dedman-center.jpg?h=420&la=en&w=300&hash=F8AE4ED42A0B9D1D918215BAC3E7EB93',
    type: 'Others',
    eventname: 'Boxing of Health',
    creator: 'Laura',
    location: 'Dedman Center',
    time: '8:00pm - 10:00pm'
  },
  {
    avatar:'https://www.smu.edu/-/media/Site/News/NewsSources/SMUat100/dedman-center.jpg?h=420&la=en&w=300&hash=F8AE4ED42A0B9D1D918215BAC3E7EB93',
    type: 'PingPong',
    eventname: 'PingPong in Crum',
    creator: 'Alisa',
    location: 'Crum',
    time: '8:00pm - 9:30pm'
  },
  {
    avatar:'https://www.smu.edu/-/media/Site/StudentAffairs/Housing/exteriors/MaryHay_200px.jpg?la=en',
    type: 'PingPong',
    eventname: 'PingPong Blast',
    creator: 'Marta',
    location: 'Dedman Center',
    time: '6:30pm - 8:00pm'
  },
  {
    avatar:'https://www.smu.edu/-/media/Site/News/NewsSources/SMUat100/dedman-center.jpg?h=420&la=en&w=300&hash=F8AE4ED42A0B9D1D918215BAC3E7EB93',
    eventname:"Tony's exercise",
    creator:"Tonny Lee",
    location:"Dedman Senter",
    time:"7:00-8:00",
    type:"Football"
  },
  {
    avatar:'https://www.smu.edu/-/media/Site/News/NewsSources/SMUat100/ford-stadium.jpg?h=225&la=en&w=300&hash=4D2DDA4F4C42F4EF793804798E65E6D1',
    eventname:"Preparation for LSNB",
    creator:"WT",
    location:"TBD",
    time:"16:00-18:00",
    type:"Football"
  },
  {
    avatar:'https://www.smu.edu/-/media/Site/News/NewsSources/SMUat100/dedman-center.jpg?h=420&la=en&w=300&hash=F8AE4ED42A0B9D1D918215BAC3E7EB93',
    eventname:"Club Interview",
    creator:"Cade Fox",
    location:"Moody studio",
    time:"20:30-22:00",
    type:"Baseball"
  },
  {
    avatar:'http://www.thesismag.com/wp-content/uploads/2014/03/SMU-DALLAS-HALL.jpg',
    eventname:"Yankees",
    creator:"bAsEbAllBigFAN",
    location:"intramural fields",
    time:"8:30am-10am",
    type:"Baseball"
  },
  {
    avatar:'https://upload.wikimedia.org/wikipedia/commons/thumb/3/3c/Westcott_Field.JPG/2880px-Westcott_Field.JPG',
    eventname:"FCB sucks",
    creator:"Yutong Li",
    location:"Westcott Field",
    time:"5:30pm-7:pm",
    type:"Soccer"
  },
  {
    avatar:'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRV1Bx76oJ0xQIku5IHN3Ri_uBT1hb36C58UccGrXJOCx3sVsUg4w&s',
    eventname:"Three points shot practices",
    creator:"Silly Shooter",
    location:"Dedman senter",
    time:"14:00-16:00",
    type:"Basketball"
  },
  {
    avatar:'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRV1Bx76oJ0xQIku5IHN3Ri_uBT1hb36C58UccGrXJOCx3sVsUg4w&s',
    eventname:"Go mustang!",
    creator:"SMU",
    location: "Moody studio",
    time:"6:00pm-8:00pm",
    type: "Basketball"
  },
  {
    avatar:'https://media.nbcdfw.com/images/652*367/Moody+-+game+7+-+photo+1.jpg',
    eventname:"LSNB",
    creator:"Jimmy Frank",
    location:"Greenville",
    time:"10:00am-12:00pm",
    type:"Basketball"
  }
];

class LoginPage extends React.Component {

  state = {
    username: '',
    password: ''
  }
  handleEmail = (text) => {
    this.setState({ username: text })
  }
  handlePassword = (text) => {
    this.setState({ password: text })
  }
  login = (username, password) => {
    return new Promise((resolve, reject) => {
      axios.get(`${url}/login/login/${username}/${password}`, config)
        .then(resp => {
          resolve(resp.data);
          if (resp.data.username) {
            user = resp.data.username;
            this.populateMyList();
            this.props.navigation.navigate('Home');
          }
          else {
            alert("Incorrect Username or Password")
          }
        })
        .catch((resp) => alert(resp));
    });
  }

  populateMyList() {
    for (var i = 0; i < eventList.length; i++) {
      if (eventList[i].creator == user) {
        myList.push(eventList[i]);
      }
    }
  }

  render() {
    let pic = {
      uri: 'http://freevector.co/wp-content/uploads/2013/12/33838-basketball-ball-variant.png'
    };
    return (

      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text style={styles.titleText} >Court Keeper</Text>
        <Image source={pic} style={{ width: 200, height: 200 }} />

        <View style={styles.container, { width: 300 }}>
          <TextInput style={styles.input}
            underlineColorAndroid="transparent"
            placeholder="Username"
            placeholderTextColor="#9a73ef"
            autoCapitalize="none"
            onChangeText={this.handleEmail} />

          <TextInput style={styles.input}
            underlineColorAndroid="transparent"
            placeholder="Password "
            placeholderTextColor="#9a73ef"
            secureTextEntry={true}

            autoCapitalize="none"
            onChangeText={this.handlePassword} />

          <TouchableOpacity
            style={styles.submitButton}
            onPress={
              () => this.login(this.state.username, this.state.password)
            }>
            <Text style={styles.submitButtonText}> Login </Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={styles.submitButton}
            onPress={
              () => this.props.navigation.navigate('Register')
            }>
            <Text style={styles.submitButtonText}> Register Here!</Text>
          </TouchableOpacity>

        </View>
      </View>

    );
  }
}


class HomePage extends React.Component {
  render() {
    return (

      <View style={{ flex: 1, flexDirection: 'column', alignItems: 'center', justifyContent: 'center' }}>

          <View style={{ flex: 1, justifyContent: 'flex-start', margin:10 }}>
            <TouchableOpacity onPress={() => this.props.navigation.navigate('ProfilePage')}>
              <Text style={styles.button}>My Profile</Text>
            </TouchableOpacity>
          </View>
          {/* <View style={{ flex: 1, justifyContent: 'center' }}>
            <TouchableOpacity onPress={() => this.props.navigation.navigate('TeamProfileScreen')}>
              <Text style={styles.button}>Team Profile</Text>
            </TouchableOpacity>
          </View> */}

          {/* <View style={{ flex: 1, justifyContent: 'center' }}>
            <TouchableOpacity onPress={() => this.props.navigation.navigate('CreateTeam')}>
              <Text style={styles.button}>Create Team</Text>
            </TouchableOpacity>
          </View> */}



          <View style={{ flex: 1, justifyContent: 'center' }}>
            <TouchableOpacity onPress={() => this.props.navigation.navigate('MyEvents')
            }>
              <Text style={styles.button}>My Events</Text>
            </TouchableOpacity>
          </View>


          <View style={{ flex: 1, justifyContent: 'center' }}>
            <TouchableOpacity onPress={() => this.props.navigation.navigate('EventSearch')}>
              <Text style={styles.button}>My Search</Text>
            </TouchableOpacity>
          </View>

          <View style={{ flex: 1, justifyContent: 'center' }}>
            <TouchableOpacity onPress={() => this.props.navigation.navigate('QRScreen')}>
              <Text style={styles.button}>My QRcode</Text>
            </TouchableOpacity>
          </View>
        </View>


    );
  }
}


class EventSearch extends React.Component {
  state = {
    search: '',
    placeholder: 'Sports',
    modalVisible: false,
    eventName: '',
    location: '',
    time: '',
    sport: ''
  }

  updateSearch = search => {
    this.setState({ search });
  }

  changePlaceHolder(e) {
    if (e == "1"){
      this.setState({ placeholder: 'Tennis'});
    }else if(e == "2"){
      this.setState({ placeholder: 'PingPong'});
    }else if(e == "3"){
      this.setState({ placeholder: 'Soccer'});
    }else if(e == "4"){
      this.setState({ placeholder: 'Basketball'});
    }else if(e == "5"){
      this.setState({ placeholder: 'Baseball'});
    }else if(e == "6"){
      this.setState({ placeholder: 'Pool'});
    }else if(e == "7"){
      this.setState({ placeholder: 'Football'});
    }else if(e == "8"){
      this.setState({ placeholder: 'Others'});
    }else {
      this.setState({ placeholder: 'Sports'})
    }
  }

  goToEvent(event) {
    currEvent = event;
    token = 'EventSearch'
    this.props.navigation.navigate('EventInfo');
  }

  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }

  joinEvent(event) {
    var isInMyList = false;
    for (var i = 0; i < myList.length; i++) {
      if (myList[i].eventname == event.eventname) {
        isInMyList = true;
      }
    }
    if (!isInMyList) {
      myList.push(event);
    }

    this.props.navigation.navigate('Home');
  }

  addEvent(visible) {
    newEvent = {
      avatar: 'https://reactnativecode.com/wp-content/uploads/2018/02/Default_Image_Thumbnail.png',
      eventname: this.state.eventName,
      creator: user,
      location: this.state.location,
      time: this.state.time
    }
    eventList.push(newEvent);
    myList.push(newEvent);
    this.setState({ modalVisible: visible });
  }

  render() {
    const { search } = this.state;


    return (
      <View style={styles.container}>
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          <View style={{ flex: 1.5, alignItems: 'center' }}>
            <ModalDropdown dropdownTextStyle={{ textAlign: 'center', fontWeight: 'bold' }}
              dropdownStyle={{ width: 80, height: 200 }}
              textStyle={{ fontSize: 15, fontWeight: 'bold', textAlign: 'center' }}
              defaultValue='Sports'
              options={['Sports', 'Tennis', 'PingPong', 'Soccer', 'Basketball', 'Baseball', 'Pool', 'Football', 'Others']}
              onSelect={(e) => this.changePlaceHolder(e)}>
            </ModalDropdown>
          </View>
          <View style={{ flex: 5, padding: 10 }}>
            <SearchBar lightTheme rounded placeholder={this.state.placeholder} onChangeText={this.updateSearch} value={this.state.search} />
          </View>
          <View style={{ flex: 0.7, alignItems: 'center' }}>
            <Modal
              style={styles.container}
              presentationStyle="pageSheet"
              animationType="slide"
              transparent={false}
              visible={this.state.modalVisible}
              onRequestClose={() => {
                Alert.alert('Modal has been closed.');
              }}>
              <View style={styles.container, { flex: 1, flexDirection: 'column', justifyContent: 'center' }}>
                <Card titleStyle={{ fontSize: 20 }} title='Create Your Own Event!'>
                  <Input label='Event Name:' placeholder='' onChangeText={(text) => this.setState({ eventName: text })} />
                  <Text></Text><Text></Text>
                  <Input label='Location:' placeholder='' onChangeText={(text) => this.setState({ location: text })} />
                  <Text></Text><Text></Text>
                  <Input label='Time Period:' placeholder='' onChangeText={(text) => this.setState({ time: text })} />
                  <Text></Text><Text></Text>
                  <Input label='Sport:' placeholder='' onChangeText={(text) => this.setState({ sport: text })} />
                  <Text></Text><Text></Text>
                  <View >
                    <Button title="Create" onPress={() => this.addEvent(!this.state.modalVisible)} />
                    <Button title="Cancel" color='red' onPress={() => this.setState({ modalVisible: false })} />
                  </View>
                </Card>
              </View>
            </Modal>
            <Ionicons name={'ios-add-circle-outline'} size={35} style={{ color: 'black' }} onPress={() => this.setModalVisible(true)} />

          </View>
        </View>
        <ScrollView>
          <View style={{ justifyContent: 'flex-start' }}>
            {eventList.map((l, i) => (
              (this.state.placeholder == l.type) ?
                (this.state.search == l.eventname) ?
                  <Card
                    key={i}
                    title={l.eventname}
                     image={{uri:l.avatar}}
                  >
                    <Text style={{ marginBottom: 10 }}>
                      Location: {l.location}
                    </Text>
                    <Text style={{ marginBottom: 10 }}>
                      Time: {l.time}
                    </Text>
                    <Button
                      icon={<Icon name='code' color='#ffffff' />}
                      buttonStyle={{ borderRadius: 0, marginLeft: 0, marginRight: 0, marginBottom: 0 }}
                      title='JOIN NOW'
                      onPress={() => this.joinEvent(
                        newEvent = {
                          avatar: l.avatar,
                          eventname: l.eventname,
                          creator: l.creator,
                          location: l.location,
                          time: l.time
                        }
                      )}
                    />
                  </Card>
                :(this.state.search == '') ?
                  <Card
                    key={i}
                    title={l.eventname}
                    image={{uri:l.avatar}}
                  >
                    <Text style={{ marginBottom: 10 }}>
                      Location: {l.location}
                    </Text>
                    <Text style={{ marginBottom: 10 }}>
                      Time: {l.time}
                    </Text>
                    <Button
                      icon={<Icon name='code' color='#ffffff' />}
                      buttonStyle={{ borderRadius: 0, marginLeft: 0, marginRight: 0, marginBottom: 0 }}
                      title='JOIN NOW'
                      onPress={() => this.joinEvent(
                        newEvent = {
                          avatar: l.avatar,
                          eventname: l.eventname,
                          creator: l.creator,
                          location: l.location,
                          time: l.time
                        }
                      )}
                    />
                  </Card>
                :<View></View>

              :(this.state.placeholder == 'Sports')?
                (this.state.search == l.eventname) ?
                  <Card
                    key={i}
                    title={l.eventname}
                     image={{uri:l.avatar}}
                  >
                    <Text style={{ marginBottom: 10 }}>
                      Location: {l.location}
                    </Text>
                    <Text style={{ marginBottom: 10 }}>
                      Time: {l.time}
                    </Text>
                    <Button
                      icon={<Icon name='code' color='#ffffff' />}
                      buttonStyle={{ borderRadius: 0, marginLeft: 0, marginRight: 0, marginBottom: 0 }}
                      title='JOIN NOW'
                      onPress={() => this.joinEvent(
                        newEvent = {
                          avatar: l.avatar,
                          eventname: l.eventname,
                          creator: l.creator,
                          location: l.location,
                          time: l.time
                        }
                      )}
                    />
                  </Card>
                :(this.state.search == '') ?
                  <Card
                    key={i}
                    title={l.eventname}
                    image={{uri:l.avatar}}
                  >
                    <Text style={{ marginBottom: 10 }}>
                      Location: {l.location}
                    </Text>
                    <Text style={{ marginBottom: 10 }}>
                      Time: {l.time}
                    </Text>
                    <Button
                      icon={<Icon name='code' color='#ffffff' />}
                      buttonStyle={{ borderRadius: 0, marginLeft: 0, marginRight: 0, marginBottom: 0 }}
                      title='JOIN NOW'
                      onPress={() => this.joinEvent(
                        newEvent = {
                          avatar: l.avatar,
                          eventname: l.eventname,
                          creator: l.creator,
                          location: l.location,
                          time: l.time
                        }
                      )}
                    />
                  </Card>
                :<View></View>
              :
              <View key={i}></View>
            ))}
          </View>
        </ScrollView>
      </View>
    );
  }
}


class ProfilePage extends React.Component {
  state = {
    editMode: false,
    avatar: 'https://reactnativecode.com/wp-content/uploads/2018/02/Default_Image_Thumbnail.png',
    name: "",
    email: "",
    newEmail: "",
    team: "NA"
  };

  editView() {
    this.setState({ editMode: true })
  }

  saveView() {
    this.setState({ editMode: false })
  }

  updateEmail(e) {
    this.setState({ newEmail: e });
    this.setState({ email: e });
    return new Promise((resolve, reject) => {
      axios.get(`${url}/profile/updateEmail/${user}/${e}`)
        .then(resp => {
          resolve(resp.data);
        })
        .catch((resp) => alert(resp));
    });
  }

  getInfo(userName) {
    return new Promise((resolve, reject) => {
      axios.get(`${url}/profile/getUserInfo/${userName}`, config)
        .then(resp => {
          resolve(resp.data);
          this.setState({ email: resp.data[0].email });
        })
        .catch((resp) => alert(resp));
    });
  }

  // logout() {
  //   user = '';
  //   this.props.navigation.navigate('Login');
  // }

  render() {
    this.getInfo(user)
    return (
      <View style={styles.container}>
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          <View style={{ flex: 1, alignItems: 'center' }}>
          </View>
          <View style={{ alignItems: 'center', flex: 5, padding: 10 }}>
            <Text style={{ fontSize: 30 }}> Your Profile </Text>
          </View>
          <View style={{ flex: 1.5, alignItems: 'center' }}>
            {!this.state.editMode && <><Button
              title="Edit"
              type="outline"
              onPress={() => this.editView()}
            /></>}
            {this.state.editMode && <><Button
              title="Save"
              onPress={() => this.saveView()}
            /></>}
          </View>
        </View>
        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-start', margin: 10 }}>
          <Image source={{ uri: this.state.avatar }} style={{ width: 150, height: 150, margin: 10 }} />
        </View >
        {!this.state.editMode && <>
          <View style={{ flexDirection: 'column', alignItems: 'flex-start', margin: 10 }}>
            <Text style={{ margin: 15, fontSize: 20 }}> Username: {user}</Text>
            <Text style={{ margin: 15, fontSize: 20 }}> Contact: {this.state.email}</Text>
          </View></>
        }
        {this.state.editMode && <>
          <View style={{ flexDirection: 'column', alignItems: 'flex-start', justifyContent: 'flex-start', margin: 10 }}>
            <View style={{ flexDirection: 'row', alignItems: 'flex-start', justifyContent: 'flex-start', margin: 10 }}>
              <Text style={{ margin: 15, fontSize: 20 }}> Username: </Text>
              <TextInput placeholder='Name'
                style={{ height: 30, borderColor: 'gray', borderWidth: 1 }}
                value={this.state.name}
                onChangeText={(e) => this.setState({ name: e.target.value })}
              />
            </View>
            <View style={{ flexDirection: 'row', alignItems: 'flex-start', justifyContent: 'flex-start', margin: 10 }}>
            <Text style={{ margin: 15, fontSize: 20 }}> Contact:  </Text>
            <TextInput placeholder='Phone'
              style={{ height: 30, borderColor: 'gray', borderWidth: 1 }}
              value={this.state.newEmail}
              onChangeText={(e) => this.updateEmail(e)}
            />
            </View>

          </View>
        </>}
      </View>
    );
  }
}


// class TeamProfilePage extends React.Component {
//   render() {
//     let pic = {
//       uri: 'https://as1.ftcdn.net/jpg/02/23/22/54/500_F_223225430_WWq0b3ruw8Pj8JFaeXB6ix3rTK9HjNK6.jpg'
//     };
//     let qr = {
//       uri: 'http://2.bp.blogspot.com/-BOeHI2tOOKU/UYmvqtskzSI/AAAAAAAAAhw/EokcaetwXZw/s320/QR-CODE.png'
//     };
//     return (
//       <View style={styles.TeamContainer}>
//         <Text style={styles.title}>
//           Team Profile
//         </Text>
//         <View style={{ flex: 1, flexDirection: "row" }}>
//           <Image source={pic} style={{ padding: 10, width: 100, height: 100, flex: 1 }} ></Image>
//           <Image source={qr} style={{ padding: 10, width: 200, height: 200, flex: 1 }} ></Image>
//         </View>
//         <View style={{ flex: 1, flexDirection: "row", alignSelf: "center" }}>
//           <View style={{ flex: 1, flexdirection: "col" }}>
//             <Text style={styles.leftcol}>Team Name:</Text>
//             <Text style={styles.leftcol}>Members:</Text>
//             <Text style={styles.leftcol}>Events:</Text>
//             <Text style={styles.leftcol}>Tags:</Text>
//           </View>
//           <View style={{ flex: 2, flexDirection: "col" }}>
//             <Text style={styles.rightcol}>SampleText</Text>
//             <Text style={styles.rightcol}>SampleText</Text>
//             <Text style={styles.rightcol}>SampleText</Text>

//             <Text style={styles.rightcol}>SampleText</Text>
//           </View>
//         </View>

//         <Button
//           title="Request to Join"
//         />
//       </View>
//     );
//   }
// }


class RegisterScreen extends React.Component {
  state = {
    firstName: '',
    lastName: '',
    userName: '',
    email: '',
    password: ''
  }
  handleFName = (text) => {
    this.setState({ firstName: text })
  }
  handleLName = (text) => {
    this.setState({ lastName: text })
  }
  handleUsername = (text) => {
    this.setState({ userName: text })
  }
  handleEmail = (text) => {
    this.setState({ email: text })
  }
  handlePassword = (text) => {
    this.setState({ password: text })
  }
  register = () => {
    userInfo = {
      firstName: this.state.firstName,
      lastName: this.state.lastName,
      userName: this.state.userName,
      password: this.state.password,
      email: this.state.email,
      qrcode: this.state.firstName
    }
    return new Promise((resolve, reject) => {
      axios.get(`${url}/login/createAccount/${userInfo.userName}/${userInfo.firstName}/
      ${userInfo.lastName}/${userInfo.password}/${userInfo.email}`, userInfo)
        .then((resp) => {
          resolve(resp.data);
          if (resp.data.username) {
            user = resp.data.username;
            this.props.navigation.navigate('Preference');
          }
          else if (resp.data == "account exists") {
            alert("This Username/Account already exists.");
          }
          else {
            alert("Issue Creating Account. Please try again later.")
          }

        })
        .catch((resp) => console.log(resp));
    });
  }

  render() {
    return (
      <KeyboardAvoidingView style={styles.container} behavior="padding" enabled keyboardVerticalOffset={200}>
        <View style={{ alignItems: 'center', justifyContent: 'center' }}>

          <Text style={styles.title}>Create an Account</Text>

          <View style={styles.container, { width: 300 }}>
            <TextInput style={styles.input}
              underlineColorAndroid="transparent"
              placeholder="First Name"
              placeholderTextColor="#9a73ef"
              autoCapitalize="none"
              onChangeText={this.handleFName} />

            <TextInput style={styles.input}
              underlineColorAndroid="transparent"
              placeholder="Last Name"
              placeholderTextColor="#9a73ef"
              autoCapitalize="none"
              onChangeText={this.handleLName} />

            <TextInput style={styles.input}
              underlineColorAndroid="transparent"
              placeholder="Username"
              placeholderTextColor="#9a73ef"
              autoCapitalize="none"
              onChangeText={this.handleUsername} />

            <TextInput style={styles.input}
              underlineColorAndroid="transparent"
              placeholder="Email"
              placeholderTextColor="#9a73ef"
              autoCapitalize="none"
              onChangeText={this.handleEmail} />

            <TextInput style={styles.input}
              underlineColorAndroid="transparent"
              placeholder="Password"
              placeholderTextColor="#9a73ef"
              secureTextEntry={true}
              autoCapitalize="none"
              onChangeText={this.handlePassword} />
          </View>
          <View style={styles.container, { width: 300 }}>
            <TouchableOpacity onPress={this.handlPress}>
              <Text style={styles.registerButton} onPress={
                () => this.register()
              }>Submit</Text>
            </TouchableOpacity>
          </View>
        </View>
      </KeyboardAvoidingView>

    );

  }
}


class Preference extends React.Component {

  state = {
    sport: 1,
    grade: 'Freshmen',
    address: '',
    onCampus: false
  }

  register = () => {
    userPreference = {
      sport: this.state.sport,
      grade: this.state.grade,
      address: this.state.address,
      onCampus: +this.state.onCampus
    }
    return new Promise((resolve, reject) => {
      axios.get(`${url}/profile/updateSport/${user}/${userPreference.sport}`, userPreference)
        .then((resp) => {
          resolve(resp.data);
        })
        .catch((resp) => console.log(resp));
      axios.get(`${url}/profile/updateGrade/${user}/${userPreference.grade}`, userPreference)
        .then((resp) => {
          resolve(resp.data);
        })
        .catch((resp) => console.log(resp));
      axios.get(`${url}/profile/updateAddress/${user}/${userPreference.address}`, userPreference)
        .then((resp) => {
          resolve(resp.data);
        })
        .catch((resp) => console.log(resp));
      axios.get(`${url}/profile/updateOnCampus/${user}/${userPreference.onCampus}`, userPreference)
        .then((resp) => {
          resolve(resp.data);
          this.props.navigation.navigate('Home');
        })
        .catch((resp) => console.log(resp));
    });
  }

  render() {
    return (
      <KeyboardAvoidingView style={styles.container} behavior="padding" enabled>
        <View >

          <Text style={{ margin: 10, fontSize: 40, fontWeight: 'bold', textAlign: 'center' }}>We need learn more about you!</Text>

          <ScrollView contentContainerStyle={{ alignItems: 'center', justifyContent: 'center' }} showsVerticalScrollIndicator={false}
            contentInset={{ top: 0, left: 0, right: 0, bottom: 100 }}>
            <Text style={{ margin: 10, fontSize: 20, textAlign: 'left' }}>Tell us what kinds of sports you like:</Text>
            <Picker style={{ height: 100, width: 200 }}
              selectedValue={this.state.sport}
              onValueChange={(itemValue, itemIndex) => this.setState({ sport: itemValue })}>
              <Picker.Item label="Football" value={1} />
              <Picker.Item label="Soccer" value={3} />
              <Picker.Item label="Basketball" value={4} />
              <Picker.Item label="Tennis" value={5} />
              <Picker.Item label="Pool" value={6} />
              <Picker.Item label="Baseball" value={2} />
              <Picker.Item label="Ping-Pong" value={8} />
              <Picker.Item label="Others" value={7} />
            </Picker>
            <Text></Text><Text></Text><Text></Text><Text></Text><Text></Text><Text></Text>
            <Text></Text><Text></Text><Text></Text>


            <Text style={{ margin: 10, fontSize: 20, textAlign: 'left' }}>Tell us what's your grade:</Text>
            <Picker style={{ height: 100, width: 200, textAlign: 'center' }}
              selectedValue={this.state.grade}
              onValueChange={(itemValue, itemIndex) => this.setState({ grade: itemValue })}>
              <Picker.Item label="Freshmen" value="Freshmen" />
              <Picker.Item label="Sophomore" value="Sophomore" />
              <Picker.Item label="Junior" value="Junior" />
              <Picker.Item label="Senior" value="Senior" />
            </Picker>
            <Text></Text><Text></Text><Text></Text><Text></Text><Text></Text><Text></Text>
            <Text></Text><Text></Text><Text></Text>

            <Text style={{ margin: 10, fontSize: 20, textAlign: 'left' }}>Tell us where you live:</Text>
            <TextInput style={{ flex: 3, height: 40, borderColor: 'gray', borderWidth: 1, margin: 10, borderRadius: 5, padding: 5, width: 300 }}
              underlineColorAndroid="transparent"
              placeholder="Your Address"
              placeholderTextColor="gray"
              autoCapitalize="none"
              onChangeText={(e) => this.setState({ address: e })} />
            <Text></Text><Text></Text>
            <Text style={{ margin: 10, fontSize: 20, textAlign: 'left' }}>Are you living on campus:</Text>
            <View style={{ margin: 10, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
              <CheckBox
                title='Yes'
                checked={this.state.onCampus}
                onPress={() => this.setState({ onCampus: true })}
              />
              {}
              <CheckBox
                title='No'
                checked={!this.state.onCampus}
                onPress={() => this.setState({ onCampus: false })}
              />
            </View>
            <Text></Text><Text></Text><Text></Text><Text></Text>

            <Button
              raised
              buttonStyle={{ width: 200, height: 50 }}
              icon={{ name: 'add-circle-outline', color: 'white' }}
              title='SUBMIT'
              onPress={() => this.register()} />
          </ScrollView>
        </View>
      </KeyboardAvoidingView>


    );

  }
}


// class CreateTeam extends React.Component {
//   state = {
//     qrText: "",
//     prevention: " "
//   }

//   render() {
//     let pic = {
//       uri: 'https://cdn4.iconfinder.com/data/icons/qigong-qi-energy-power-chakra/316/qi-gong-power-003-512.png'
//     };
//     return (
//       <View style={styles.TeamContainer}>
//         <Text style={styles.title}>
//           Creating Team
//         </Text>
//         <View style={{ flex: 1, flexDirection: "row" }}>
//           <Image source={pic} style={{ padding: 10, width: 200, height: 200, flex: 1 }} ></Image>
//           <QRCode value={this.state.prevention + this.state.qrText} />
//         </View>
//         <View style={{ flex: 1, flexDirection: "row" }}>
//           <View style={{ flex: 1, flexDirection: "col", alignSelf: "center" }}>
//             <Text>Team Name</Text>
//             <TextInput
//               style={{ height: 40, borderColor: 'gray', borderWidth: 1, padding: 10 }}
//               onChangeText={(text) => this.setState({ qrText: `${text}` })}
//               value={this.state.qrText}
//             />
//             <Text>Team Member</Text>
//             <TextInput
//               style={{ height: 40, borderColor: 'gray', borderWidth: 1, padding: 10 }}
//             />
//             <Text>Team Description</Text>
//             <TextInput
//               style={{ height: 40, borderColor: 'gray', borderWidth: 1, padding: 10 }}
//             />
//             <Text>Tags</Text>
//             <TextInput
//               style={{ height: 40, borderColor: 'gray', borderWidth: 1, padding: 10 }}
//             />
//           </View>
//         </View>



//         <Button
//           title="QUIT"
//           color="red"
//         />
//       </View>
//     );
//   }

// }


class MyEvents extends React.Component {
  state = {
    search: '',
  };

  updateSearch = search => {
    this.setState({ search });
  };

  goToEvent(event) {
    currEvent = event;
    token = 'myEvents';
    this.props.navigation.navigate('EventInfo');
  }

  render() {
    const { search } = this.state;


    return (
      <View style={styles.container}>
        <ScrollView>
          <View style={{ justifyContent: 'flex-start' }}>
            {
              myList.map((l, i) => (
                <Card
                  key={i}
                  containerStyle={{ borderColor: (user == l.creator) ? 'red' : 'blue' }}
                  title={l.eventname}
                  image={{ uri: l.avatar }}
                >
                  <Text style={{ marginBottom: 10 }}>
                    Location: {l.location}
                  </Text>
                  <Text style={{ marginBottom: 10 }}>
                    Time: {l.time}
                  </Text>

                  <Button
                    icon={<Icon name='code' color='#ffffff' />}
                    buttonStyle={{ borderRadius: 0, marginLeft: 0, marginRight: 0, marginBottom: 0 }}
                    title='Check Info'
                    onPress={() => this.goToEvent(l)} />
                </Card>

              ))
            }
          </View>
        </ScrollView>
      </View>
    );
  }
}


class EventEditPage extends React.Component {
  render() {
    let pic = {
      uri: 'http://freevector.co/wp-content/uploads/2013/12/33838-basketball-ball-variant.png'
    };
    let qr = {
      uri: 'http://2.bp.blogspot.com/-BOeHI2tOOKU/UYmvqtskzSI/AAAAAAAAAhw/EokcaetwXZw/s320/QR-CODE.png'
    };
    return (
      <View style={styles.containerEditPage}>
        <Text style={styles.title}>
          Event Name
        </Text>
        <View style={{ flex: 1, flexDirection: "row" }}>
          <Image source={pic} style={{ padding: 10, width: 200, height: 200, flex: 1 }} ></Image>
          <Image source={qr} style={{ padding: 10, width: 200, height: 200, flex: 1 }} ></Image>
        </View>
        <View style={{ flex: 1, flexDirection: "row" }}>
          <View style={{ flex: 1, flexDirection: "col", alignSelf: "center" }}>
            <Text>Location</Text>
            <TextInput
              style={{ height: 40, borderColor: 'gray', borderWidth: 1, padding: 10 }}
            />
            <Text>Time</Text>
            <TextInput
              style={{ height: 40, borderColor: 'gray', borderWidth: 1, padding: 10 }}
            />
            <Text>Details</Text>
            <TextInput
              style={{ height: 40, borderColor: 'gray', borderWidth: 1, padding: 10 }}
            />
            <Text>Tags</Text>
            <TextInput
              style={{ height: 40, borderColor: 'gray', borderWidth: 1, padding: 10 }}
            />
          </View>
        </View>


        <Button
          title="Manage Join Requests"
        />
        <Button
          title="Close Event"
          color="red"
        />
      </View>
    )
  }
}


class QRScreen extends React.Component {
  state = {
    text: 'http://facebook.github.io/react-native/',
    isVisible:true
  };
  onPress(){
    this.props.navigation.navigate('HomePage');
    this.setState({isVisible:false})
  }
  onQR(){
    this.setState({isVisible:true});
  }
  render() {
    return (
      <>
      <View style={styles.containerQR}>
      <TouchableHighlight onPress={()=> this.onQR()}>
        <Text style= {{  fontSize: 30, fontWeight: 'bold', textAlign: 'center', padding: 20}}>Personal QR Code</Text>
      </TouchableHighlight>
         <QRCode
           value={user}
           color={'#7a42f4'}
           size={300}
         />

      </View>
      <View style={styles.containerQR}>
     <Overlay isVisible={this.state.isVisible}
     windowBackgroundColor="grey"
     overlayBackgroundColor="white"
     isVisible={this.state.isVisible}
     height= "auto"
     width = "auto"
     onBackdropPress={()=>this.onPress()}>
       <Text style= {{  fontSize: 30, fontWeight: 'bold', textAlign: 'center', padding: 20}}>Personal QR Code</Text>
        <QRCode
          value={user}
          color={'#7a42f4'}
          size={300}
        />
    </Overlay>
      </View>



      </>
    );
  };
}


class EventInfo extends React.Component {
  deleteEvent(event) {
    for (var i = 0; i < eventList.length; i++) {
      if (eventList[i].eventname == currEvent.eventname) {
        eventList.splice(i, 1);
        break;
      }
    }
    for (var i = 0; i < myList.length; i++) {
      if (myList[i].eventname == currEvent.eventname) {
        myList.splice(i, 1);
        break;
      }
    }
    this.props.navigation.navigate('Home');
  }
  joinEvent(event) {
    var isInMyList = false;
    for (var i = 0; i < myList.length; i++) {
      if (myList[i].eventname == currEvent.eventname) {
        isInMyList = true;
      }
    }
    if (!isInMyList) {
      myList.push(currEvent);
    }
    this.props.navigation.navigate('Home');
  }

  render() {
    let pic = {
      uri: currEvent.avatar
    };
    let qr = {
      uri: 'http://2.bp.blogspot.com/-BOeHI2tOOKU/UYmvqtskzSI/AAAAAAAAAhw/EokcaetwXZw/s320/QR-CODE.png'
    };
    return (
      <View style={styles.containerEditPage}>
        <Text style={styles.title}>
          {currEvent.eventname}
        </Text>
        <View style={{ flex: 1, flexDirection: "row" }}>
          <Image source={pic} style={{ padding: 10, width: 200, height: 200, flex: 1 }} ></Image>
        </View>
        <View style={{ flex: 1, flexDirection: "row", alignSelf: "center" }}>
          <View style={{ flex: 1, flexdirection: "col" }}>
            <Text style={styles.leftcol}>Creator:</Text>
            <Text style={styles.leftcol}>Location:</Text>
            <Text style={styles.leftcol}>Time:</Text>
          </View>
          <View style={{ flex: 2, flexDirection: "col" }}>
            <Text style={styles.rightcol}>{currEvent.creator}</Text>
            <Text style={styles.rightcol}>{currEvent.location}</Text>
            <Text style={styles.rightcol}>{currEvent.time}</Text>
          </View>
        </View>

        {token != 'myEvents' && <Button
          disabled={(user == currEvent.creator) ? true : false}
          title="Join Event"
          onPress={(currEvent) => this.joinEvent(currEvent)}
        />}
        {token == 'myEvents' && <Button
          disabled={(user == currEvent.creator) ? false : true}
          title="End Event"
          onPress={(currEvent) => this.deleteEvent(currEvent)}
        />}
      </View>
    )
  }
}


const RootStack = createStackNavigator(
  {
    Login: {
      screen: LoginPage,
      navigationOptions: () => ({
        title: `Login Page`,
        headerBackTitle: null,
      }),
    },

    Home: {
      screen: HomePage,
      navigationOptions: () => ({
        title: `Home Page`,
        headerBackTitle: null,
        headerLeft: null,
        gesturesEnabled: false
      }),
    },
    Preference: {
      screen: Preference,
      navigationOptions: () => ({
        title: `More Info`,
        headerBackTitle: null,
        headerLeft: null,
        gesturesEnabled: false
      }),
    },

    EventEditAdd: {
      screen: EventEditPage,
      navigationOptions: () => ({
        title: `Event Management`,
        headerBackTitle: null
      })
    },

    Register: {
      screen: RegisterScreen,
      navigationOptions: () => ({
        title: `Register Page`,
        headerBackTitle: null,
      }),
    },

    // CreateTeam: {
    //   screen: CreateTeam,
    //   navigationOptions: () => ({
    //     title: `Create Team Page`,
    //     headerBackTitle: null,
    //   }),
    // },

    // TeamProfileScreen: {
    //   screen: TeamProfilePage,
    //   navigationOptions: () => ({
    //     title: `Team Profile Screen`,
    //     headerBackTitle: null,
    //   }),
    // },

    ProfilePage: {
      screen: ProfilePage,
      navigationOptions: () => ({
        title: `Your Profile`,
        headerBackTitle: null,
      }),
    },

    EventEdit: {
      screen: EventEditPage,
      navigationOptions: () => ({
        title: `Event Edit Page`,
        headerBackTitle: null,
      }),
    },

    MyEvents: {
      screen: MyEvents,
      navigationOptions: () => ({
        title: `Events Page`,
        headerBackTitle: null,
      }),
    },

    EventSearch: {
      screen: EventSearch,
      navigationOptions: () => ({
        title: `Events Page`,
        headerBackTitle: null,
      }),
    },

    EventInfo: {
      screen: EventInfo,
      navigationOptions: () => ({
        title: `Events Info`,
        headerBackTitle: null,
      }),
    },
    QRScreen: {
      screen: QRScreen,
      navigationOptions: () => ({
        title: `QR`,
        headerBackTitle: null,
      }),
    },
  },
  {
    initialRouteName: 'Login',
  });



const AppContainer = createAppContainer(RootStack);

export default class App extends React.Component {
  render() {
    return <AppContainer />;
  }
}

//export default createAppContainer(TabNavigator);

{/* Style SHeets */ }
const styles = StyleSheet.create({
  container: {

    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  TeamContainer: {
    flex: 1,
    justifyContent: 'center',
    paddingTop: Constants.statusBarHeight,
    backgroundColor: '#ecf0f1',
    padding: 8,
  },

  titleText: {
    fontSize: 30,
    fontWeight: 'bold',
  },
  input: {
    margin: 15,
    height: 40,
    borderColor: '#7a42f4',
    borderWidth: 1
  },
  submitButton: {
    backgroundColor: '#7a42f4',
    padding: 10,
    margin: 15,
    height: 40
  },
  submitButtonText: {
    color: 'white'
  },
  button: {
    backgroundColor: '#7a42f4',
    borderColor: 'white',
    borderWidth: 4,
    borderRadius: 17,
    color: 'white',
    fontSize: 25,
    fontWeight: 'bold',
    overflow: 'hidden',
    padding: 50,
    height: 200,
    textAlign: 'center',
  },
  registerButton: {
    backgroundColor: '#7a42f4',
    borderColor: 'white',
    borderWidth: 4,
    borderRadius: 17,
    color: 'white',
    fontSize: 24,
    fontWeight: 'bold',
    overflow: 'hidden',
    padding: 30,
    textAlign: 'center',
  },
  header: {
    height: 50,
    backgroundColor: 'steelblue'
  },
  buttonwrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  card: {
    height: 90,
    width: 250,
    backgroundColor: 'lightgray',
    alignSelf: 'center',
    flexDirection: 'row',
    borderWidth: 0.5,
    borderColor: 'black'
  },
  textView: {
    justifyContent: 'center',
    alignItems: 'flex-start'
  },
  containerEditPage: {
    flex: 1,
    justifyContent: 'center',
    paddingTop: Constants.statusBarHeight,
    backgroundColor: '#ecf0f1',
    padding: 8,
  },
  title: {
    margin: 24,
    fontSize: 40,
    fontWeight: 'bold',
    textAlign: 'center'
  },
  leftcol: {
    margin: 10,
    flex: 1,
    fontSize: 20,
    fontWeight: "bold",
    textAlign: "right"
  },
  rightcol: {
    margin: 10,
    flex: 1,
    fontSize: 20,
    textAlign: "left"
  },
  containerQR: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center'
  },
  input: {
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
    margin: 10,
    borderRadius: 5,
    padding: 5,
  },

});

//AppRegistry.registerComponent('QRScreen', () => QRScreen);
