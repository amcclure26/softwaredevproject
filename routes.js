import axios from 'react-native-axios'

export class Routes {
    url = "nameless-everglades-94891.herokuapp.com";
    config = {

    };

    registerUser(user) {
        return new Promise((resolve, reject) => {
            axios.post(`${this.url}/login/createAccount/${user.userName}/${user.firstName}/${user.lastName}/${user.password}/${user.email}/${user.qrcode}`, user, this.config)
                .then(resp => resolve(resp.data))
                .catch(resp => alert(resp));
        });
    }

    loginUser(username,password) {
        return new Promise((resolve,reject) => {
            axios.get(`${this.url}/login/login/${username}/${password}`)
                .then(resp => resolve(resp.data))
                .catch(resp => alert(resp));
        });
    }
}

export default Routes;